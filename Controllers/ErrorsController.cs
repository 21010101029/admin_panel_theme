﻿using Microsoft.AspNetCore.Mvc;

namespace AdminPanel_Theme.Controllers
{
    public class ErrorsController : Controller
    {
        [Route("Errors/{statusCode}")]
        public IActionResult HandleError(int statusCode)
        {
            if (statusCode == 404)
            {
                return View("404");
            }

            // Handle other errors as needed
            return View("Error"); // Create an "Error.cshtml" view for other errors
        }
    }
}

﻿using AdminPanel_Theme.BAL;
using AdminPanel_Theme.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace AdminPanel_Theme.Controllers
{
    //[CheckAccess]
    public class HomeController : Controller
    {

        public IActionResult Index()
        {
            return View("IndexUser");
        }

        [CheckAccess]
        public IActionResult IndexUser()
        {
            return View("Index");
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View();
        }


    }
}
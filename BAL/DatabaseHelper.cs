﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using AdminPanel_Theme.DAL;

namespace AdminPanel_Theme.BAL
{
    public class DatabaseHelper:DALHelper
    {
        SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
        #region ExecuteSelectAll
        public DataTable ExecuteSelectAll(string procedureName)
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand(procedureName);
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }
                return dt;
            }
            catch (Exception ex)
            {
                // Log or handle the exception as needed
                return null;
            }
        }

        #endregion

        #region ExecuteDelete
        public void ExecuteDelete(string procedureName, int? ID,String columnName)
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand(procedureName);
                sqlDatabase.AddInParameter(dbCommand, $"@{columnName}", DbType.String, ID);
                // Additional parameters can be added as needed
                // sqlDatabase.AddInParameter(dbCommand, "@UserID", DbType.Int32, CV.UserID());
                sqlDatabase.ExecuteNonQuery(dbCommand);
            }
            catch (Exception ex)
            {
                // Log or handle the exception as needed
            }
        }
        #endregion

        #region ExecuteSelectByPK
        public T ExecuteSelectByPK<T>(string procedureName, int? primaryKeyValue,String columnName) where T : new()
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand(procedureName);
                sqlDatabase.AddInParameter(dbCommand, $"@{columnName}", DbType.String, primaryKeyValue);
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }

                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    return MapDataRowToObject<T>(dr);
                }
                return default(T);
            }
            catch (Exception ex)
            {
                // Log or handle the exception as needed
                return default(T);
            }
        }

        

        private T MapDataRowToObject<T>(DataRow dr) where T : new()
        {
            T model = new T();
            foreach (var property in typeof(T).GetProperties())
            {
                if (dr.Table.Columns.Contains(property.Name) && dr[property.Name] != DBNull.Value)
                {
                    property.SetValue(model, dr[property.Name]);
                }
            }
            return model;
        }
        #endregion'

        #region ExecuteSaveRecord
        public bool ExecuteSaveRecord<T>(string procedureName, T model,string columnName) where T : new()
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand;

                if (GetPrimaryKeyValue(model) == null)
                {
                    dbCommand = sqlDatabase.GetStoredProcCommand(procedureName);
                    SetParameters(dbCommand, model);
                }
                else
                {
                    dbCommand = sqlDatabase.GetStoredProcCommand(procedureName);
                    SetParameters(dbCommand, model);
                    sqlDatabase.AddInParameter(dbCommand, $"@{columnName}", DbType.Int64, GetPrimaryKeyValue(model));
                }

                // Additional parameters can be added as needed
                // sqlDatabase.AddInParameter(dbCommand, "@UserID", DbType.Int32, CV.UserID());

                sqlDatabase.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch (Exception ex)
            {
                // Log or handle the exception as needed
                return false;
            }
        }
        private object GetPrimaryKeyValue<T>(T model)
        {
            // Replace with the logic to get the primary key value from the model
            // For simplicity, assuming the primary key property is named "ID"
            var property = typeof(T).GetProperty("ID");
            return property?.GetValue(model);
        }
        private void SetParameters<T>(DbCommand dbCommand, T model)
        {
            foreach (var property in typeof(T).GetProperties())
            {
                sqlDatabase.AddInParameter(dbCommand, $"@{property.Name}", GetDbType(property.PropertyType), property.GetValue(model) ?? DBNull.Value);
            }
        }
        private DbType GetDbType(Type type)
        {
            // Replace with the logic to map C# types to DbType
            // This is a basic implementation; you might need to enhance it
            if (type == typeof(int) || type == typeof(int?))
            {
                return DbType.Int32;
            }
            else if (type == typeof(string))
            {
                return DbType.String;
            }
            else if (type == typeof(DateTime) || type == typeof(DateTime?))
            {
                return DbType.DateTime;
            }
            // Add more types as needed

            throw new NotSupportedException($"Type {type} is not supported.");
        }
        #endregion
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace AdminPanel_Theme.Areas.FC_Program.Models
{
    public class FC_ProgramModel
    {
        public int ProgramID { get; set; }
        [Required]
        public string ProgramFullName { get; set; }
        [Required]
        public string ProgramShortName { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
    }

    public class FC_ProgramInsertModel
    {
        [Required]
        public string ProgramFullName { get; set; }
        [Required]
        public string ProgramShortName { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
    }

    public class FC_ProgramUpdateModel
    {
        public int ProgramID { get; set; }
        [Required]
        public string ProgramFullName { get; set; }
        [Required]
        public string ProgramShortName { get; set; }
        public DateTime? Modified { get; set; }
    }
}

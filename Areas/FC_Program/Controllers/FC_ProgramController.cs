﻿using AdminPanel_Theme.Areas.FC_Program.Models;
using AdminPanel_Theme.DAL.FC_ProgramDAL;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace AdminPanel_Theme.Areas.FC_Program.Controllers
{
    [Area("FC_Program")]
    [Route("FC_Program/[controller]/[action]")]
    public class FC_ProgramController : Controller
    {
        private readonly FC_ProgramDAL _dalFC_Program;

        public FC_ProgramController()
        {
            _dalFC_Program = new FC_ProgramDAL(); // Assuming you have a parameterless constructor for FC_ProgramDAL
        }

        #region FC_ProgramList
        public IActionResult FC_ProgramList()
        {
            List<FC_ProgramModel> models = new List<FC_ProgramModel>();
            DataTable dt = _dalFC_Program.PR_FC_Program_SelectAll();
            return View(dt);
        }
        #endregion

        #region Add
        public IActionResult Add(int? ProgramID)
        {
            if (ProgramID == null)
            {
                return View("AddEditProgram");
            }
            else
            {
                FC_ProgramModel model = _dalFC_Program.PR_FC_Program_SelectByPK(ProgramID);
                return View("AddEditProgram", model);
            }
        }
        #endregion

        #region AddEditProgram
        [HttpPost]
        public IActionResult AddEditProgram(FC_ProgramModel model)
        {
            if (_dalFC_Program.SaveProgram(model))
            {
                return RedirectToAction("FC_ProgramList");
            }

            return View();
        }
        #endregion

        #region Delete
        public IActionResult Delete(int? ProgramID)
        {
            // Perform any additional logic before deleting, if needed
            _dalFC_Program.PR_FC_Program_Delete(ProgramID);
            return RedirectToAction("FC_ProgramList");
        }
        #endregion
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace AdminPanel_Theme.Areas.City.Models
{
    public class LOC_CityModel
    {
        public int? CityID { get; set; }
        [Required]
        public int? StateID { get; set; }
        [Required]
        public string CityName { get; set; }

        [Required]
        public string CityCode { get; set; }
        public DateTime Created { get; set; } 
        public DateTime Modified { get; set; }
        [Required]
        public string StateName { get; set; }
        public string StateCode { get; set; }
    }


    public class LOC_CityInsertModel
    { 
        public int? StateID { get; set; }
        public string CityName { get; set; }
        public string CityCode { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
    }


    public class LOC_CityUpdateModel
    {
        public int? CityID { get; set; }
        public int? StateID { get; set; }
        public string CityName { get; set; }
        public string CityCode { get; set; }
        public DateTime? Modified { get; set; }
    }
    public class LOC_DropDownState
    {
        public int? StateID { get; set; }
        public string StateName { get; set; }
    }
    public class LOC_CityFilterModel
    {
        public int? CountryID { get; set; }
        public int? StateID { get; set; }
        public string? CityName { get; set; }
        public string? CityCode { get; set; }
    }
}

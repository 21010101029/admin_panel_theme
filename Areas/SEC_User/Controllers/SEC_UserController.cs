﻿using AdminPanel_Theme.Areas.SEC_User.Models;
using AdminPanel_Theme.BAL;
using AdminPanel_Theme.DAL.SEC_UserDAL;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace AdminPanel_Theme.Areas.SEC_User.Controllers
{
    //[CheckAccess]
    [Area("SEC_User")]
    [Route("SEC_User/[controller]/[action]")]
    public class SEC_UserController : Controller
    {
        public IActionResult SEC_UserLogin()    
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(SEC_UserModel modelSEC_User)
        {
            string error = null;
            Console.WriteLine("Hello ",modelSEC_User.UserName);
            if (modelSEC_User.UserName == null)
            {
                error += "User Name is required";
            }
            if (modelSEC_User.Password == null)
            {
                error += "<br/>Password is required";
            }

            if (error != null)
            {
                TempData["Error"] = error;
                return RedirectToAction("SEC_UserLogin");
            }
            else
            {
                SEC_UserDAL dal = new SEC_UserDAL();
                DataTable dt = dal.dbo_PR_SEC_User_SelectByUserNamePassword(modelSEC_User.UserName, modelSEC_User.Password);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Console.WriteLine(dr["IsAdmin"].ToString());

                        HttpContext.Session.SetString("UserName", dr["UserName"].ToString());
                        HttpContext.Session.SetString("UserID", dr["UserID"].ToString());
                        HttpContext.Session.SetString("Password", dr["Password"].ToString());
                        HttpContext.Session.SetString("FirstName", dr["FirstName"].ToString());
                        HttpContext.Session.SetString("LastName", dr["LastName"].ToString());
                        HttpContext.Session.SetString("PhotoPath", dr["PhotoPath"].ToString());
                        HttpContext.Session.SetString("IsAdmin", dr["IsAdmin"].ToString());
                        break;
                    }
                }
                else
                {
                    TempData["Error"] = "User Name or Password is invalid!";
                    return RedirectToAction("SEC_UserLogin");
                }
                if (HttpContext.Session.GetString("UserName") != null && HttpContext.Session.GetString("Password") != null)
                {
                    if (CV.IsAdmin())
                    {
                        return RedirectToAction("IndexUser", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index","Home");
                    }
                    
                }
            }
            return RedirectToAction("Index");
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return new RedirectResult("~/Home/Index");

            //if (CV.IsAdmin())
            //{
            //}
            //else
            //{
            //    return RedirectToAction("IndexUser", "Home");
            //}

        }
    }
}

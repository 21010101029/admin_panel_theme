﻿using System.ComponentModel.DataAnnotations;

namespace AdminPanel_Theme.Areas.FC_UniversityType.Models
{
    public class FC_UniversityTypeModel
    {
        public int UniversityTypeID { get; set; }

        [Required]
        public string UniversityTypeName { get; set; }

        [Required]
        public string Remark { get; set; }

        [Required]
        public string UniversityDisplayName { get; set; }

        [Required]
        public int Sequence { get; set; }

        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
    }

    public class FC_UniversityTypeInsertModel
    {
        [Required]
        public string UniversityTypeName { get; set; }

        [Required]
        public string Remark { get; set; }

        [Required]
        public string UniversityDisplayName { get; set; }

        [Required]
        public int Sequence { get; set; }

        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
    }

    public class FC_UniversityTypeUpdateModel
    {
        [Required]
        public string UniversityTypeName { get; set; }

        [Required]
        public string Remark { get; set; }

        [Required]
        public string UniversityDisplayName { get; set; }

        [Required]
        public int Sequence { get; set; }
        public DateTime? Modified { get; set; }
    }


}

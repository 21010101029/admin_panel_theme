﻿using System.ComponentModel.DataAnnotations;

namespace AdminPanel_Theme.Areas.States.Models
{
    public class LOC_StateModel
    {
        public int StateID { get; set; }
        [Required]
        public string StateName { get; set; }
        [Required]
        public string StateCode { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
    }

    public class LOC_StateInsertModel
    {
        [Required]
        public string StateName { get; set; }
        [Required]
        public string StateCode
        {
            get; set;
        }
        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }
    }


    public class LOC_StateUpdateModel
    {
        public int StateID { get; set; }
        [Required]
        public string StateName { get; set; }
        [Required]
        public string StateCode { get; set; }
        public DateTime? Modified { get; set; }
    }
    public class LOC_StateFilterModel
    {
        public string? StateName { get; set; }
        public string? StateCode { get; set; }
        public int? CountryID { get; set; }
    }
}


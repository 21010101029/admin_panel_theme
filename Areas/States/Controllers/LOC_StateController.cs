﻿using AdminPanel_New.DAL.LOC_StateDAL;
using AdminPanel_Theme.Areas.States.Models;
using AdminPanel_Theme.BAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System.Data;

namespace AdminPanel_Theme.Areas.States.Controllers
{
    [CheckAccess]
    [Area("States")]
    [Route("States/[controller]/[action]")]
    public class LOC_StateController : Controller
    {
        LOC_StateDAL dalLOC_StateDAL = new LOC_StateDAL();
        #region LOC_StateList
        public IActionResult LOC_StateList()
        {
            List<LOC_StateModel> models = new List<LOC_StateModel>();
            DataTable dt = dalLOC_StateDAL.dbo_PR_LOC_State_SelectAll();
            return View(dt);
        }
        #endregion

        #region ADD
        public IActionResult Add(int? StateID)
        {
            if (StateID == null)
            {
                //ViewBag.CountryList = dalLOC_StateDAL.DropdownCountry();
                return View("AddEditState");
            }
            else
            {
                LOC_StateModel model = dalLOC_StateDAL.PR_LOC_State_SelectByPK(StateID);
                //ViewBag.CountryList = dalLOC_StateDAL.DropdownCountry();
                return View("AddEditState", model);
            }

        }
        #endregion

        #region ADD & Edit State
        public IActionResult AddEditState(LOC_StateModel model)
        {
            LOC_StateDAL dalLOC_StateDAL = new LOC_StateDAL();

            if (dalLOC_StateDAL.SaveState(model))
            {
                return RedirectToAction("LOC_StateList");
            }

            return View();
        }
        #endregion

        #region Delete
        public IActionResult Delete(int? StateID)
        {
            dalLOC_StateDAL.PR_LOC_deleteCity_FollowedBy_States(StateID);
            dalLOC_StateDAL.PR_LOC_State_Delete(StateID);
            return RedirectToAction("LOC_StateList");
        }
        #endregion

        #region SearchState
        public IActionResult SearchState(string StateName)
        {
            DataTable dt = dalLOC_StateDAL.PR_State_SelectByStateName(StateName);
            return View("LOC_StateList", dt);
        }
        #endregion

        #region LOC_StateFilter
        public IActionResult LOC_StateFilter(LOC_StateFilterModel filterModel)
        {
            DataTable dt=dalLOC_StateDAL.PR_State_Filter(filterModel);
            ViewBag.CountryList = dalLOC_StateDAL.DropdownCountry();
            ModelState.Clear();
            return View("LOC_StateList", dt);

        }
        #endregion
    }
}

﻿using AdminPanel_Theme.Areas.Country.Models;
using AdminPanel_Theme.BAL;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data;
using System.Data.Common;

namespace AdminPanel_New.DAL.LOC_CountryDAL
{
    public class LOC_CountryDAL:LOC_CountryDALBase
    {
        #region dbo_PR_LOC_deleteState_FollowedBy_Country
        public void PR_LOC_deleteState_FollowedBy_Country(int? CountryID)
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_deleteState_FollowedBy_Country");
                sqlDatabase.AddInParameter(dbCommand, "@CountryID", DbType.Int64, CountryID);
                sqlDatabase.ExecuteNonQuery(dbCommand);
                return;
            }
            catch (Exception ex)
            {
                return;
            }
        }
        #endregion

        #region dbo_PR_LOC_deleteCity_FollowedBy_Country
        public void PR_LOC_deleteCity_FollowedBy_Country(int? CountryID)
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_deleteCity_FollowedBy_Country");
                sqlDatabase.AddInParameter(dbCommand, "@CountryID", DbType.Int64, CountryID);
                sqlDatabase.ExecuteNonQuery(dbCommand);
                return;
            }
            catch (Exception ex)
            {
                return;
            }
        }
        #endregion

        #region dbo.PR_Country_SelectByCountryName
        public DataTable PR_Country_SelectByCountryName(string CountryName)
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_Country_SelectByCountryName");
                sqlDatabase.AddInParameter(dbCommand, "@CountryName", DbType.String, CountryName);
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }
                return dt;
            }
            catch (Exception e) { return null; }
        }
        #endregion

        #region [PR_Country_Filter]
        public DataTable PR_Country_Filter(LOC_CountryFilterModel filterModel)
        {
            DataTable dt=new DataTable();
            SqlDatabase sqlDatabase=new SqlDatabase(myConnectionString);
            DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_Country_Filter");
            sqlDatabase.AddInParameter(dbCommand, "@CountryName",DbType.String, filterModel.CountryName);
            sqlDatabase.AddInParameter(dbCommand, "@CountryCode", DbType.String, filterModel.CountryCode);
            //sqlDatabase.AddInParameter(dbCommand, "@UserID", DbType.Int32, CV.UserID());
            using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
            {
                dt.Load(reader);
            }
            return dt;

        }
        #endregion
    }
}

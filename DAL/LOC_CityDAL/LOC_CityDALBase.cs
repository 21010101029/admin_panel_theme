﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data;
using AdminPanel_Theme.DAL;
using AdminPanel_Theme.Areas.City.Models;
using AdminPanel_Theme.BAL;
using AdminPanel_Theme.Areas.States.Models;

namespace AdminPanel_New.DAL.LOC_CityDAL
{
    public class LOC_CityDALBase : DatabaseHelper
    {
        #region dbo_PR_LOC_City_SelectAll
        public DataTable dbo_PR_LOC_City_SelectAll()
        {
            return ExecuteSelectAll("PR_LOC_City_SelectAll");
        }
        #endregion

        #region dbo_PR_LOC_City_Insert & PR_LOC_City_Update
        public bool SaveCity(LOC_CityModel model)
        {
            try
            {
                if (model.CityID == null)
                {
                    LOC_CityInsertModel modelInsert= new LOC_CityInsertModel();
                    modelInsert.StateID = model.StateID;
                    modelInsert.CityName = model.CityName;
                    modelInsert.CityCode= model.CityCode;
                    modelInsert.Created = null;
                    modelInsert.Modified= null;
                    return ExecuteSaveRecord<LOC_CityInsertModel>("PR_LOC_City_Insert", modelInsert, "CityID");
                }
                else
                {
                    LOC_CityUpdateModel modelUpdate = new LOC_CityUpdateModel();
                    modelUpdate.CityID=model.CityID;
                    modelUpdate.StateID = model.StateID;
                    modelUpdate.CityName = model.CityName;
                    modelUpdate.CityCode = model.CityCode;
                    modelUpdate.Modified = null;
                    return ExecuteSaveRecord<LOC_CityUpdateModel>("PR_LOC_City_Update", modelUpdate, "CityID");
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region dbo_PR_LOC_City_SelectByPK
        public LOC_CityModel PR_LOC_City_SelectByPK(int? CityID)
        {
            return ExecuteSelectByPK<LOC_CityModel>("PR_LOC_City_SelectByPK", CityID, "CityID");
        }
        #endregion

        #region dbo_PR_LOC_City_Delete
        public void PR_LOC_City_Delete(int? CityID)
        {
            ExecuteDelete("PR_LOC_City_Delete", CityID, "CityID");
            return;
        }
        #endregion
    }
}

﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data;
using AdminPanel_Theme.Areas.City.Models;
using AdminPanel_Theme.BAL;
using Microsoft.AspNetCore.Mvc.ApplicationModels;


namespace AdminPanel_New.DAL.LOC_CityDAL
{
    public class LOC_CityDAL:LOC_CityDALBase
    {
        #region dbo_PR_LOC_State_SelectComboBox
        public List<LOC_DropDownState> DropDownState()
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_State_SelectComboBox");
                //sqlDatabase.AddInParameter(dbCommand, "@UserID", DbType.Int32, CV.UserID());
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }
                List<LOC_DropDownState> ListOfState = new List<LOC_DropDownState>();
                foreach (DataRow dr in dt.Rows)
                {
                    LOC_DropDownState lOC_DropDownState = new LOC_DropDownState();
                    lOC_DropDownState.StateID = Convert.ToInt32(dr["StateID"]);
                    lOC_DropDownState.StateName = dr["StateName"].ToString();
                    ListOfState.Add(lOC_DropDownState);
                }
                return ListOfState;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region dbo_PR_LOC_State_SelectComboBoxByCountryID
        public List<LOC_DropDownState> PR_LOC_State_SelectComboBoxByCountryID(int? CountryID)
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_LOC_State_SelectComboBoxByCountryID");
                sqlDatabase.AddInParameter(dbCommand, "@CountryID", DbType.Int32, CountryID);
                //sqlDatabase.AddInParameter(dbCommand, "@UserID", DbType.Int32, CV.UserID());
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }
                List<LOC_DropDownState> ListOfState = new List<LOC_DropDownState>();
                foreach (DataRow dr in dt.Rows)
                {
                    LOC_DropDownState lOC_DropDownState = new LOC_DropDownState();
                    lOC_DropDownState.StateID = Convert.ToInt32(dr["StateID"]);
                    lOC_DropDownState.StateName = dr["StateName"].ToString();
                    ListOfState.Add(lOC_DropDownState);
                }
                return ListOfState;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region dbo_PR_City_SelectByCityName
        public DataTable PR_City_SelectByCityName(string CityName)
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_City_SelectByCityName");
                sqlDatabase.AddInParameter(dbCommand, "@CName", DbType.String, CityName);
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
                {
                    dt.Load(reader);
                }
                return dt;
            }
            catch (Exception e) { return null; }
        }
        #endregion

        #region PR_City_Filter
        public DataTable PR_City_Filter(LOC_CityFilterModel filterModel)
        {
            DataTable dt = new DataTable();
            SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
            DbCommand dbCommand = sqlDatabase.GetStoredProcCommand("PR_City_Filter");
            sqlDatabase.AddInParameter(dbCommand, "@CountryID", DbType.Int32, filterModel.CountryID);
            sqlDatabase.AddInParameter(dbCommand, "@StateID", DbType.Int32, filterModel.StateID);
            sqlDatabase.AddInParameter(dbCommand, "@CityName", DbType.String, filterModel.CityName);
            sqlDatabase.AddInParameter(dbCommand, "@CityCode", DbType.String, filterModel.CityCode);
            //Console.WriteLine(filterModel.CountryID);
            //Console.WriteLine(filterModel.StateID);

            //Console.WriteLine(filterModel.CityName);
            //Console.WriteLine(filterModel.CityCode);

            sqlDatabase.AddInParameter(dbCommand, "@UserID", DbType.Int32, CV.UserID());
            using (IDataReader reader = sqlDatabase.ExecuteReader(dbCommand))
            {
                dt.Load(reader);
            }
            return dt;
        }
        #endregion
    }
}

﻿using AdminPanel_Theme.Areas.FC_Program.Models;
using AdminPanel_Theme.BAL;
using System.Data;

namespace AdminPanel_Theme.DAL.FC_ProgramDAL
{
    public class FC_ProgramDALBase:DatabaseHelper
    {
        #region PR_FC_UniversityWiseProgram_SelectAll
        public DataTable PR_FC_Program_SelectAll()
        {
            return ExecuteSelectAll("PR_FC_Program_SelectAll");
        }
        #endregion

        #region dbo_PR_FC_Program_Insert & PR_FC_Program_Update
        public bool SaveProgram(FC_ProgramModel model)
        {
            if (model.ProgramID == 0)
            {
                FC_ProgramInsertModel insertModel = new FC_ProgramInsertModel
                {
                    ProgramFullName = model.ProgramFullName,
                    ProgramShortName = model.ProgramShortName,
                    Created = model.Created,
                    Modified = model.Modified
                };

                return ExecuteSaveRecord<FC_ProgramInsertModel>("PR_FC_Program_Insert", insertModel, "ProgramID");
            }
            else
            {
                FC_ProgramUpdateModel updateModel = new FC_ProgramUpdateModel
                {
                    ProgramID = model.ProgramID,
                    ProgramFullName = model.ProgramFullName,
                    ProgramShortName = model.ProgramShortName,
                    Modified = model.Modified
                };

                return ExecuteSaveRecord<FC_ProgramUpdateModel>("PR_FC_Program_Update", updateModel, "ProgramID");
            }
        }
        #endregion

        #region dbo_PR_FC_Program_Delete
        public void PR_FC_Program_Delete(int? programID)
        {
            if (programID == null)
            {
                return;
            }
            ExecuteDelete("PR_FC_Program_Delete", programID, "ProgramID");
        }
        #endregion

        #region dbo_PR_FC_Program_SelectByPK
        public FC_ProgramModel PR_FC_Program_SelectByPK(int? programID)
        {
            return ExecuteSelectByPK<FC_ProgramModel>("PR_FC_Program_SelectByPK", programID, "ProgramID");
        }
        #endregion

    }
}

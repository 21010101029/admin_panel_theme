﻿using AdminPanel_Theme.Areas.FC_UniversityType.Models;
using AdminPanel_Theme.BAL;
using System.Data;

namespace AdminPanel_Theme.DAL.FC_UniversityDAL
{
    public class FC_UniversityTypeDALBase : DatabaseHelper
    {

        #region PR_FC_UniversityType_SelectAll
        public DataTable PR_FC_UniversityType_SelectAll()
        {
            return ExecuteSelectAll("PR_FC_UniversityType_SelectAll");
        }
        #endregion

        #region dbo_PR_FC_UniversityType_Insert & PR_FC_UniversityType_Update
        public bool SaveUniversityType(FC_UniversityTypeModel model)
        {
            if (model.UniversityTypeID == 0)
            {
                FC_UniversityTypeInsertModel insertModel = new FC_UniversityTypeInsertModel
                {
                    UniversityTypeName = model.UniversityTypeName,
                    Remark = model.Remark,
                    Sequence = model.Sequence,
                    UniversityDisplayName=model.UniversityDisplayName,
                    Created = model.Created,
                    Modified = model.Modified
                };

                return ExecuteSaveRecord<FC_UniversityTypeInsertModel>("PR_FC_UniversityType_Insert", insertModel, "UniversityTypeID");
            }
            else
            {
                FC_UniversityTypeUpdateModel updateModel = new FC_UniversityTypeUpdateModel
                {
                    Sequence=model.Sequence,
                    UniversityDisplayName = model.UniversityDisplayName,
                    UniversityTypeName = model.UniversityTypeName,
                    Remark = model.Remark,
                    Modified = model.Modified
                };

                return ExecuteSaveRecord<FC_UniversityTypeUpdateModel>("PR_FC_UniversityType_Update", updateModel, "UniversityTypeID");
            }
        }
        #endregion

        #region dbo_PR_FC_UniversityType_Delete
        public void PR_FC_UniversityType_Delete(int? universityTypeID)
        {
            if (universityTypeID == null)
            {
                return;
            }
            ExecuteDelete("PR_FC_UniversityType_Delete", universityTypeID, "UniversityTypeID");
        }
        #endregion

        #region dbo_PR_FC_UniversityType_SelectByPK
        public FC_UniversityTypeModel PR_FC_UniversityType_SelectByPK(int? universityTypeID)
        {
            return ExecuteSelectByPK<FC_UniversityTypeModel>("PR_FC_UniversityType_SelectByPK", universityTypeID, "UniversityTypeID");
        }
        #endregion


    }
}

﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data;
using System.Drawing.Printing;
using AdminPanel_Theme.Areas.States.Models;
using AdminPanel_Theme.DAL;
using AdminPanel_Theme.BAL;

namespace AdminPanel_New.DAL.LOC_StateDAL
{
    public class LOC_StateDALBase:DatabaseHelper
    {
        #region dbo_PR_LOC_State_SelectAll
        public DataTable dbo_PR_LOC_State_SelectAll()
        {
            return ExecuteSelectAll("PR_LOC_State_SelectAll");
        }
        #endregion

        #region dbo_PR_LOC_State_Insert & PR_LOC_State_Update
        public bool SaveState(LOC_StateModel modelLOC_StateModel)
        {
            
            if (modelLOC_StateModel.StateID == 0)
            {
                LOC_StateInsertModel model = new LOC_StateInsertModel();
                model.StateName = modelLOC_StateModel.StateName;
                model.StateCode = modelLOC_StateModel.StateCode;
                return ExecuteSaveRecord<LOC_StateInsertModel>("PR_LOC_State_Insert", model, "StateID");
                    
            }
            else
            {
                LOC_StateUpdateModel model = new LOC_StateUpdateModel();
                model.StateID = modelLOC_StateModel.StateID;
                model.StateName = modelLOC_StateModel.StateName;
                model.StateCode = modelLOC_StateModel.StateCode;
                model.Modified = modelLOC_StateModel.Modified;
                return ExecuteSaveRecord<LOC_StateUpdateModel>("PR_LOC_State_Update", model, "StateID");

            }

        }
        #endregion

        #region dbo_PR_LOC_State_Delete
        public void PR_LOC_State_Delete(int? stateID)
        {
            if (stateID == null)
            {
                return;
            }
            ExecuteDelete("PR_LOC_State_Delete", stateID, "StateID");


        }
        #endregion

        #region dbo_PR_LOC_State_SelectByPK

        public LOC_StateModel PR_LOC_State_SelectByPK(int? StateID)
        {
            return ExecuteSelectByPK<LOC_StateModel>("PR_LOC_State_SelectByPK", StateID, "StateID");
        }
        #endregion

    }
}
